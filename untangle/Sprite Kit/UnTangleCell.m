//
//  UnTangleCell.m
//  untangle
//
//  Created by Dat Nguyen on 3/17/14.
//  Copyright (c) 2014 Dat Nguyen. All rights reserved.
//

#import "UnTangleCell.h"
#import "UnTangleTile.h"

@implementation UnTangleCell

- (instancetype)initWithPosition:(UnTanglePosition)position
{
  if (self = [super init]) {
    self.position = position;
    self.tile = nil;
  }
  return self;
}


- (void)setTile:(UnTangleTile *)tile
{
  _tile = tile;
  if (tile) tile.cell = self;
}

@end
