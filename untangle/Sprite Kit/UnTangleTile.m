//
//  UnTangleTile.m
//  untangle
//
//  Created by Dat Nguyen on 4/16/14.
//  Copyright (c) 2014 Dat Nguyen. All rights reserved.
//

#include <stdlib.h>

#import "UnTangleTile.h"
#import "UnTangleCell.h"
#import "UnTangleGlobalState.h"
static NSString * const kchip1 = @"chip1";
static NSString * const kchip2 = @"chip2";

@implementation UnTangleTile {
  /** The value of the tile, as some text. */
  SKLabelNode *_value;
  
  /** Pending actions for the tile to execute. */
  NSMutableArray *_pendingActions;
}


# pragma mark - Tile creation

+ (UnTangleTile *)insertNewTileToCell:(UnTangleCell *)cell
{
  UnTangleTile *tile = [[UnTangleTile alloc] init];
  
  // The initial position of the tile is at the center of its cell. This is so because when
  // scaling the tile, SpriteKit does so from the origin, not the center. So we have to scale
  // the tile while moving it back to its normal position to achieve the "pop out" effect.
  CGPoint origin = [GSTATE locationOfPosition:cell.position];
  tile.position = CGPointMake(origin.x + GSTATE.tileSize / 2, origin.y + GSTATE.tileSize / 2);
  [tile setScale:0];
  
  cell.tile = tile;
  return tile;
}


- (instancetype)init
{
  if (self = [super init]) {
    // Layout of the tile.
    CGRect rect = CGRectMake(0, 0, GSTATE.tileSize, GSTATE.tileSize);
    CGPathRef rectPath = CGPathCreateWithRoundedRect(rect, GSTATE.cornerRadius, GSTATE.cornerRadius, NULL);
    self.path = rectPath;
    CFRelease(rectPath);
    self.lineWidth = 0;
    
    // Initiate pending actions queue.
    _pendingActions = [[NSMutableArray alloc] init];
    
    // Set up value label.
    _value = [SKLabelNode labelNodeWithFontNamed:[GSTATE boldFontName]];
    _value.position = CGPointMake(GSTATE.tileSize / 2, GSTATE.tileSize / 2);
    _value.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    _value.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    [self addChild:_value];
    

    
    [self refreshValue];
  }
  return self;
}


# pragma mark - Public methods

- (void)removeFromParentCell
{
  // Check if the tile is still registered with its parent cell, and if so, remove it.
  // We don't really care about self.cell, because that is a weak pointer.
  if (self.cell.tile == self) self.cell.tile = nil;
}



- (void)commitPendingActions
{
  [self runAction:[SKAction sequence:_pendingActions]];
  [_pendingActions removeAllObjects];
}




- (void)refreshValue
{
  _value.text = [NSString stringWithFormat:@"Chip"];
  _value.fontColor = [GSTATE textColorForLevel:1];
  _value.fontSize = 18;
  
  self.fillColor = [GSTATE colorForLevel:1];
}


- (void)moveToCell:(UnTangleCell *)cell
{
  [_pendingActions addObject:[SKAction moveBy:[GSTATE distanceFromPosition:self.cell.position
                                                                toPosition:cell.position]
                                     duration:GSTATE.animationDuration]];
  self.cell.tile = nil;
  cell.tile = self;
}


- (void)removeAnimated:(BOOL)animated
{
  [self removeFromParentCell];
  // @TODO: fade from center.
  if (animated) [_pendingActions addObject:[SKAction scaleTo:0 duration:GSTATE.animationDuration]];
  [_pendingActions addObject:[SKAction removeFromParent]];
  [self commitPendingActions];
}


- (void)removeWithDelay
{
  [self removeFromParentCell];
  SKAction *wait = [SKAction waitForDuration:GSTATE.animationDuration];
  SKAction *remove = [SKAction removeFromParent];
  [self runAction:[SKAction sequence:@[wait, remove]]];
}


# pragma mark - SKAction helpers

- (SKAction *)pop
{
  CGFloat d = 0.15 * GSTATE.tileSize;
  SKAction *wait = [SKAction waitForDuration:GSTATE.animationDuration / 3];
  SKAction *enlarge = [SKAction scaleTo:1.3 duration:GSTATE.animationDuration / 1.5];
  SKAction *move = [SKAction moveBy:CGVectorMake(-d, -d) duration:GSTATE.animationDuration / 1.5];
  SKAction *restore = [SKAction scaleTo:1 duration:GSTATE.animationDuration / 1.5];
  SKAction *moveBack = [SKAction moveBy:CGVectorMake(d, d) duration:GSTATE.animationDuration / 1.5];
  
  return [SKAction sequence:@[wait, [SKAction group:@[enlarge, move]],
                                    [SKAction group:@[restore, moveBack]]]];
}

@end
