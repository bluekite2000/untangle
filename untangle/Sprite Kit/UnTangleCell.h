//
//  UnTangleCell.h
//  untangle
//
//  Created by Dat Nguyen on 3/17/14.
//  Copyright (c) 2014 Dat Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UnTanglePosition.h"
@class UnTangleTile;

@interface UnTangleCell : NSObject

/** The position of the cell. */
@property (nonatomic) UnTanglePosition position;

/** The tile in the cell, if any. */
@property (nonatomic, strong) UnTangleTile *tile;

/**
 * Initialize a UnTangleCell at the specified position with no tile in it.
 *
 * @param position The position of the cell.
 */
- (instancetype)initWithPosition:(UnTanglePosition)position;

@end
