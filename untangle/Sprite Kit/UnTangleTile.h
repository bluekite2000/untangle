//
//  UnTangleTile.h
//  untangle
//
//  Created by Dat Nguyen on 4/16/14.
//  Copyright (c) 2014 Dat Nguyen. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@class UnTangleCell;

@interface UnTangleTile : SKShapeNode

/** The level of the tile. */
@property (nonatomic) NSInteger level;

/** The cell this tile belongs to. */
@property (nonatomic, weak) UnTangleCell *cell;

/**
 * Creates and inserts a new tile at the specified cell.
 *
 * @param cell The cell to insert tile into.
 * @return The tile created.
 */
+ (UnTangleTile *)insertNewTileToCell:(UnTangleCell *)cell;

- (void)commitPendingActions;





/**
 * Moves the tile to the specified cell. If the tile is not already in the grid, 
 * calling this method would result in error.
 *
 * @param cell The destination cell.
 */
- (void)moveToCell:(UnTangleCell *)cell;


/**
 * Removes the tile from its cell and from the scene.
 *
 * @param animated If YES, the removal will be animated.
 */
- (void)removeAnimated:(BOOL)animated;

@end
