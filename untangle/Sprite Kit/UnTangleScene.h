//
//  UnTangleScene.h
//  untangle
//

//  Copyright (c) 2014 Dat Nguyen. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@class UnTangleGrid;
@class UnTangleViewController;

@interface UnTangleScene : SKScene

@property (nonatomic, weak) UnTangleViewController *delegate;

- (void)startNewGame;

- (void)loadBoardWithGrid:(UnTangleGrid *)grid;

@end
