//
//  main.m
//  untangle
//
//  Created by Dat Nguyen on 4/26/14.
//  Copyright (c) 2014 Dat Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UnTangleAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([UnTangleAppDelegate class]));
    }
}
