//
//  UnTangleSettingsViewController.h
//  untangle
//
//  Created by Dat Nguyen on 4/16/14.
//  Copyright (c) 2014 Dat Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnTangleSettingsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end
