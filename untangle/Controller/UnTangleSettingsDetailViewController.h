//
//  UnTangleSettingsDetailViewController.h
//  untangle
//
//  Created by Dat Nguyen on 3/24/14.
//  Copyright (c) 2014 Dat Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnTangleSettingsDetailViewController : UITableViewController

@property (nonatomic, strong) NSArray *options;
@property (nonatomic, strong) NSString *footer;

@end
