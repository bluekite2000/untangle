//
//  UnTangleViewController.h
//  untangle
//

//  Copyright (c) 2014 Dat Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface UnTangleViewController : UIViewController

- (void)updateScore:(NSInteger)score;

- (void)endGame:(BOOL)won;

@end
