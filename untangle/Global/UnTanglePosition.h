//
//  UnTanglePosition.h
//  untangle
//
//  Created by Dat Nguyen on 3/19/14.
//  Copyright (c) 2014 Dat Nguyen. All rights reserved.
//

#ifndef UnTanglePosition_h
#define UnTanglePosition_h

typedef struct Position {
  NSInteger x;
  NSInteger y;
} UnTanglePosition;

CG_INLINE UnTanglePosition UnTanglePositionMake(NSInteger x, NSInteger y)
{
  UnTanglePosition position;
  position.x = x; position.y = y;
  return position;
}

#endif
