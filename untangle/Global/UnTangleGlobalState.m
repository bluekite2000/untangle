//
//  UnTangleGlobalState.m
//  untangle
//
//  Created by Dat Nguyen on 4/16/14.
//  Copyright (c) 2014 Dat Nguyen. All rights reserved.
//

#import "UnTangleGlobalState.h"
#import "UnTangleTheme.h"

#define kTheme     @"Theme"
#define kBestScore @"Best Score"

@interface UnTangleGlobalState ()

@property (nonatomic, readwrite) NSInteger dimension;
@property (nonatomic, readwrite) NSInteger winningLevel;
@property (nonatomic, readwrite) NSInteger tileSize;
@property (nonatomic, readwrite) NSInteger borderWidth;
@property (nonatomic, readwrite) NSInteger cornerRadius;
@property (nonatomic, readwrite) NSInteger horizontalOffset;
@property (nonatomic, readwrite) NSInteger verticalOffset;
@property (nonatomic, readwrite) NSTimeInterval animationDuration;
@property (nonatomic) NSInteger theme;

@end


@implementation UnTangleGlobalState

+ (UnTangleGlobalState *)state
{
  static UnTangleGlobalState *state = nil;
  
  static dispatch_once_t once;
  dispatch_once(&once, ^{
    state = [[UnTangleGlobalState alloc] init];
  });
  
  return state;
}


- (instancetype)init
{
  if (self = [super init]) {
    [self setupDefaultState];
    [self loadGlobalState];
  }
  return self;
}

- (void)setupDefaultState
{
  NSDictionary *defaultValues = @{
                                  kTheme: @0,
                                  
                                  kBestScore: @0,
                                  };
  [Settings registerDefaults:defaultValues];
}

- (void)loadGlobalState
{
  self.dimension = 5;
  self.borderWidth = 5;
  self.cornerRadius = 4;
  self.animationDuration = 0.1;
  self.horizontalOffset = [self horizontalOffset];
  self.verticalOffset = [self verticalOffset];
  self.theme = [Settings integerForKey:kTheme];
  self.needRefresh = NO;
}


- (NSInteger)tileSize
{
  if (self.dimension <= 4) {
    return 66;
  }
  return 56;
}


- (NSInteger)horizontalOffset
{
  CGFloat width = self.dimension * (self.tileSize + self.borderWidth) + self.borderWidth;
  return ([[UIScreen mainScreen] bounds].size.width - width) / 2;
}


- (NSInteger)verticalOffset
{
  CGFloat height = self.dimension * (self.tileSize + self.borderWidth) + self.borderWidth + 120;
  return ([[UIScreen mainScreen] bounds].size.height - height) / 2;
}








- (NSInteger)valueForLevel:(NSInteger)level
{
    return 1;
 
}


# pragma mark - Appearance

- (UIColor *)colorForLevel:(NSInteger)level
{
  return [[UnTangleTheme themeClassForType:self.theme] colorForLevel:level];
}


- (UIColor *)textColorForLevel:(NSInteger)level
{
  return [[UnTangleTheme themeClassForType:self.theme] textColorForLevel:level];
}


- (CGFloat)textSizeForValue:(NSInteger)value
{
  NSInteger offset = self.dimension == 5 ? 2 : 0;
  if (value < 100) {
    return 32 - offset;
  } else if (value < 1000) {
    return 28 - offset;
  } else if (value < 10000) {
    return 24 - offset;
  } else if (value < 100000) {
    return 20 - offset;
  } else if (value < 1000000) {
    return 16 - offset;
  } else {
    return 13 - offset;
  }
}

- (UIColor *)backgroundColor
{
  return [[UnTangleTheme themeClassForType:self.theme] backgroundColor];
}


- (UIColor *)scoreBoardColor
{
  return [[UnTangleTheme themeClassForType:self.theme] scoreBoardColor];
}


- (UIColor *)boardColor
{
  return [[UnTangleTheme themeClassForType:self.theme] boardColor];
}


- (UIColor *)buttonColor
{
  return [[UnTangleTheme themeClassForType:self.theme] buttonColor];
}


- (NSString *)boldFontName
{
  return [[UnTangleTheme themeClassForType:self.theme] boldFontName];
}


- (NSString *)regularFontName
{
  return [[UnTangleTheme themeClassForType:self.theme] regularFontName];
}

# pragma mark - Position to point conversion

- (CGPoint)locationOfPosition:(UnTanglePosition)position
{
  return CGPointMake([self xLocationOfPosition:position] + self.horizontalOffset,
                     [self yLocationOfPosition:position] + self.verticalOffset);
}


- (CGFloat)xLocationOfPosition:(UnTanglePosition)position
{
  return position.y * (GSTATE.tileSize + GSTATE.borderWidth) + GSTATE.borderWidth;
}


- (CGFloat)yLocationOfPosition:(UnTanglePosition)position
{
  return position.x * (GSTATE.tileSize + GSTATE.borderWidth) + GSTATE.borderWidth;
}


- (CGVector)distanceFromPosition:(UnTanglePosition)oldPosition toPosition:(UnTanglePosition)newPosition
{
  CGFloat unitDistance = GSTATE.tileSize + GSTATE.borderWidth;
  return CGVectorMake((newPosition.y - oldPosition.y) * unitDistance,
                      (newPosition.x - oldPosition.x) * unitDistance);
}

@end
