//
//  UnTangleOverlay.h
//  untangle
//
//  Created by Dat Nguyen on 3/25/14.
//  Copyright (c) 2014 Dat Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnTangleOverlay : UIView

@property (nonatomic, weak) IBOutlet UILabel *message;

@property (nonatomic, weak) IBOutlet UIButton *keepPlaying;

@property (nonatomic, weak) IBOutlet UIButton *restartGame;

@end
