//
//  UnTangleScoreView.h
//  untangle
//
//  Created by Dat Nguyen on 3/23/14.
//  Copyright (c) 2014 Dat Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnTangleScoreView : UIView

@property (nonatomic, weak) IBOutlet UILabel *title;
@property (nonatomic, weak) IBOutlet UILabel *score;

/** Updates the appearance of subviews and itself. */
- (void)updateAppearance;

@end
