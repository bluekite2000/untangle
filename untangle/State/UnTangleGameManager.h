//
//  UnTangleGameManager.h
//  untangle
//
//  Created by Dat Nguyen on 4/16/14.
//  Copyright (c) 2014 Dat Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UnTangleScene;
@class UnTangleGrid;

typedef NS_ENUM(NSInteger, UnTangleDirection) {
  UnTangleDirectionUp,
  UnTangleDirectionLeft,
  UnTangleDirectionDown,
  UnTangleDirectionRight
};

@interface UnTangleGameManager : NSObject

/**
 * Starts a new session with the provided scene.
 *
 * @param scene The scene in which the game happens.
 */
- (void)startNewSessionWithScene:(UnTangleScene *)scene;

/**
 * Moves all movable tiles to the desired direction, then add one more tile to the grid.
 * Also refreshes score and checks game status (won/lost).
 *
 * @param direction The direction of the move (up, right, down, left).
 */
- (void)moveToDirection:(UnTangleDirection)direction;

@end
