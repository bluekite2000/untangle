//
//  UnTangleGameManager.m
//  untangle
//
//  Created by Dat Nguyen on 4/16/14.
//  Copyright (c) 2014 Dat Nguyen. All rights reserved.
//

#import "UnTangleGameManager.h"
#import "UnTangleGrid.h"
#import "UnTangleTile.h"
#import "UnTangleScene.h"
#import "UnTangleViewController.h"
#import "UnTangleGlobalState.h"
/**
 * Helper function that checks the termination condition of either counting up or down.
 *
 * @param value The current i value.
 * @param countUp If YES, we are counting up.
 * @param upper The upper bound of i.
 * @param lower The lower bound of i.
 * @return YES if the counting is still in progress. NO if it should terminate.
 */
BOOL iterate(NSInteger value, BOOL countUp, NSInteger upper, NSInteger lower) {
  return countUp ? value < upper : value > lower;
}


@implementation UnTangleGameManager {
  /* True if game over. */
  BOOL _over;
  
  /* True if won game. */
  BOOL _won;
  
  /* True if user chooses to keep playing after winning. */
  BOOL _keepPlaying;
  
  /* The current score. */
  NSInteger _score;
  
  /* The points earned by the user in the current round. */
  NSInteger _pendingScore;
  
  /* The grid on which everything happens. */
  UnTangleGrid *_grid;
}


# pragma mark - Setup

- (void)startNewSessionWithScene:(UnTangleScene *)scene
{
  if (_grid && _grid.dimension == GSTATE.dimension) {
    // If there is an existing grid and its dimension is still valid,
    // we keep it, only removing all existing tiles with animation.
    [_grid removeAllTilesAnimated:YES];
  } else {
    if (_grid) [_grid removeAllTilesAnimated:NO];
    _grid = [[UnTangleGrid alloc] initWithDimension:GSTATE.dimension];
    _grid.scene = scene;
  }
  
//  [scene loadBoardWithGrid:_grid];
  
  // Set the initial state for the game.
  _score = 0; _over = NO; _won = NO; _keepPlaying = NO;

  // Add two tiles to the grid to start with.
  [_grid insertTileAtRandomAvailablePositionWithDelay:NO];
}


# pragma mark - Actions

- (void)moveToDirection:(UnTangleDirection)direction
{
  __block UnTangleTile *tile = nil;
  
  // Remember that the coordinate system of SpriteKit is the reverse of that of UIKit.
  BOOL reverse = direction == UnTangleDirectionUp || direction == UnTangleDirectionRight;
  NSInteger unit = reverse ? 1 : -1;
  
  if (direction == UnTangleDirectionUp || direction == UnTangleDirectionDown) {
    [_grid forEach:^(UnTanglePosition position) {
      if ((tile = [_grid tileAtPosition:position])) {
        // Find farthest position to move to.
        NSInteger target = position.x;
     //   for (NSInteger i = position.x + unit; iterate(i, reverse, _grid.dimension, -1); i += unit) {
         
          NSInteger i = position.x + unit;
          UnTangleTile *t = [_grid tileAtPosition:UnTanglePositionMake(i, position.y)];
          
          // Empty cell; we can move at least to here.
          if (!t) target = i;
          
       
     //   }
        
        // The current tile is movable.
        if (target != position.x) {
          [tile moveToCell:[_grid cellAtPosition:UnTanglePositionMake(target, position.y)]];
          _pendingScore++;
        }
      }
    } reverseOrder:reverse];
  }
  
  else {
    [_grid forEach:^(UnTanglePosition position) {
      if ((tile = [_grid tileAtPosition:position])) {
        NSInteger target = position.y;
       // for (NSInteger i = position.y + unit; iterate(i, reverse, _grid.dimension, -1); i += unit) {
          NSInteger i = position.y + unit;
          UnTangleTile *t = [_grid tileAtPosition:UnTanglePositionMake(position.x, i)];
          
          if (!t) target = i;

       // }
        
        // The current tile is movable.
        if (target != position.y) {
          [tile moveToCell:[_grid cellAtPosition:UnTanglePositionMake(position.x, target)]];
          _pendingScore++;
        }
      }
    } reverseOrder:reverse];
  }
  
  // Cannot move to the given direction. Abort.
  if (!_pendingScore) return;
  
  // Commit tile movements.
  [_grid forEach:^(UnTanglePosition position) {
    UnTangleTile *tile = [_grid tileAtPosition:position];
    if (tile) {
      [tile commitPendingActions];
    }
  } reverseOrder:reverse];
  
  // Increment score.
  [self materializePendingScore];
  
  // Check post-move status.
  if (!_keepPlaying && _won) {
    // We set `keepPlaying` to YES. If the user decides not to keep playing,
    // we will be starting a new game, so the current state is no longer relevant.
    _keepPlaying = YES;
    [_grid.scene.delegate endGame:YES];
  }
    

    
 
}


# pragma mark - Score

- (void)materializePendingScore
{
  _score += _pendingScore;
  _pendingScore = 0;
  [_grid.scene.delegate updateScore:_score];
}


# pragma mark - State checkers




@end
